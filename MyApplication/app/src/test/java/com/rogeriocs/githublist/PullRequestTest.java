package com.rogeriocs.githublist;

import com.rogeriocs.githublist.models.PullRequest;
import com.rogeriocs.githublist.models.viewModels.PullRequestView;
import com.rogeriocs.githublist.models.viewModels.RepositorieViewModel;
import com.rogeriocs.githublist.test.HelperMockModels;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;

/**
 * Created by rogerio on 20/12/16.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class PullRequestTest {
    PullRequest pullRequest;
    PullRequestView pullRequestView;
    private PullRequestView pullRequestView2;
    private PullRequest pullRequest2;

    @Before
    public void setUp(){
        pullRequest = HelperMockModels.createMockPullRequest();
        pullRequestView = new PullRequestView(RuntimeEnvironment.application, pullRequest);
        pullRequest2 = HelperMockModels.createMockPullRequest();
        pullRequest2.setUser(null);
        pullRequestView2 = new PullRequestView(RuntimeEnvironment.application, pullRequest2);
    }

    @Test
    public void testTitlePullRequest(){
        String value = pullRequestView.getTitle();
        assertMsg(value, pullRequest.getTitle());
    }
    @Test
    public void testNameUser(){
        String value = pullRequestView.getUserName();
        assertMsg(value, pullRequest.getUser().getLogin());
    }

    @Test
    public void testNameUserEmpty(){
        String value = pullRequestView2.getUserName();
        assertMsg(value,"");
    }

    private void assertMsg(String value, String value2) {
        assertEquals("Erro  ="+value,value, value2);
    }


}
