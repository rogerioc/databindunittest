package com.rogeriocs.githublist;

import com.rogeriocs.githublist.models.Item;
import com.rogeriocs.githublist.models.viewModels.RepositorieViewModel;
import com.rogeriocs.githublist.test.HelperMockModels;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class RepositorieViewModelTest {
    private RepositorieViewModel repositorieViewModel;
    private Item item;

    @Test
    public void setUp() {
        item = HelperMockModels.createMockRepositorie();
       repositorieViewModel = new RepositorieViewModel(RuntimeEnvironment.application, item);
    }

    @Test
    public void shouldGetRepositorieName() throws Exception {
        assertEquals("Erro name ="+repositorieViewModel.getName(),repositorieViewModel.getName(), item.getName());
    }

    @Test
    public void shouldGetRepositorieDescription() throws Exception {
        assertEquals("Erro name ="+repositorieViewModel.getDescription(),repositorieViewModel.getDescription(), item.getDescription());
    }

    @Test
    public void shouldGetRepositorieOwnerName() throws Exception {
        assertEquals("Erro name ="+repositorieViewModel.getOwnerName(),repositorieViewModel.getOwnerName(), item.getOwner().getLogin());
    }

    @Test
    public void shouldGetRepositorieOwnerImage() throws Exception {
        assertEquals("Erro name ="+repositorieViewModel.getOwnerImage(),repositorieViewModel.getOwnerImage(), item.getOwner().getAvatar());
    }
}