package com.rogeriocs.githublist.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rogerio on 16/12/16.
 */
public class Searches {
    @SerializedName("total_count")
    private Long totalCount;
    @SerializedName("incomplete_results")
    private Boolean incompleteResults;
    private List<Item> items = null;

    public Long getTotalCount() {
        return totalCount;
    }

    public Searches setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
        return this;
    }

    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    public Searches setIncompleteResults(Boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
        return this;
    }

    public List<Item> getItems() {
        return items;
    }

    public Searches setItems(List<Item> items) {
        this.items = items;
        return this;
    }
}
