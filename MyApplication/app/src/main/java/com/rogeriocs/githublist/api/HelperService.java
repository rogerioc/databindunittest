package com.rogeriocs.githublist.api;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HelperService {

    private static final String PROD_URL_SSL ="https://api.github.com/";
    private static final int MAX_TRY_COUNT = 3;
    private static final int RETRY_BACKOFF_DELAY = 1000;
    private static String tag=HelperService.class.getSimpleName();
    private static Retrofit retrofit;


    public static Service setUrlToTest(String url){
        retrofit = getRetrofit(url);
        return retrofit.create(Service.class);
    }

    public static Service initRetrofit(Service service){
        retrofit = getRetrofit(PROD_URL_SSL);
        if(service==null)
        service = retrofit.create(Service.class);
        return service;
    }

    public static Service initRetrofit(){
        retrofit = getRetrofit(PROD_URL_SSL);
        return retrofit.create(Service.class);
    }


    public static Retrofit getRetrofit() {
        return retrofit;
    }

    public static void setRetrofit(Retrofit retrofit) {
        HelperService.retrofit = retrofit;
    }

    @NonNull
    private static Retrofit getRetrofit(String baseUrl) {
        OkHttpClient defaultHttpClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Request request = chain.request();

                                // try the request
                                Response response = null;
                                int tryCount = 1;
                                while (tryCount <= MAX_TRY_COUNT) {
                                    try {
                                        response = chain.proceed(request);
                                        break;
                                    } catch (Exception e) {

                                        Log.e(tag,"Erro  -> "+e.getMessage());
                                        try {
                                            Log.e(tag, "on url -> " + response.request().url());
                                        }catch (NullPointerException eNull){

                                        }
                                        if ("Canceled".equalsIgnoreCase(e.getMessage())) {
                                            // Request canceled, do not retry
                                            throw e;
                                        }
                                        if (tryCount >= MAX_TRY_COUNT) {
                                            // max retry count reached, giving up
                                            throw e;
                                        }

                                        try {
                                            // sleep delay * try count (e.g. 1st retry after 3000ms, 2nd after 6000ms, etc.)
                                            Thread.sleep(RETRY_BACKOFF_DELAY * tryCount);
                                        } catch (InterruptedException e1) {
                                            throw new RuntimeException(e1);
                                        }
                                        tryCount++;
                                    }
                                }
                                return response;
                            }
                        })
                .build();
        return new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(defaultHttpClient)
                    .build();
    }

}
