package com.rogeriocs.githublist.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.rogeriocs.githublist.R;
import com.rogeriocs.githublist.adapter.PullRequestsAdapter;
import com.rogeriocs.githublist.adapter.RepositoriesAdapter;
import com.rogeriocs.githublist.api.GitHubApi;
import com.rogeriocs.githublist.databinding.ActivityPullsrequestBinding;
import com.rogeriocs.githublist.models.Item;
import com.rogeriocs.githublist.models.PullRequest;
import com.rogeriocs.githublist.models.viewModels.RepositorieViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullsRequestsActivity extends AppCompatActivity {

    public static final String ITEM = "ITEM";
    @BindView(R.id.pull_requests)
    RecyclerView pullRequestsList;
    private Unbinder unbinder;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent != null) {
            Item item = (Item) intent.getSerializableExtra(ITEM);
            ActivityPullsrequestBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pullsrequest);
            binding.setRepositorieView(new RepositorieViewModel(this,item));
            unbinder = ButterKnife.bind(this);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            layoutManager= new LinearLayoutManager(this);
            pullRequestsList.setLayoutManager(layoutManager);

            GitHubApi.getListPullRequests(item.getOwner().getLogin(), item.getName(), new Callback<ArrayList<PullRequest>>() {
                @Override
                public void onResponse(Call<ArrayList<PullRequest>> call, Response<ArrayList<PullRequest>> response) {
                    ArrayList<PullRequest> pullRequests = response.body();
                    if(pullRequests!=null) {
                        PullRequestsAdapter pullRequestsAdapter = new PullRequestsAdapter(PullsRequestsActivity.this, pullRequests);
                        pullRequestsList.setAdapter(pullRequestsAdapter);
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<PullRequest>> call, Throwable t) {

                }
            });
        }else {
            finish();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


}
