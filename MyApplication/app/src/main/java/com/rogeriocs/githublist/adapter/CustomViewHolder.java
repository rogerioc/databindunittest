package com.rogeriocs.githublist.adapter;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import com.rogeriocs.githublist.databinding.ItemRepositorieBinding;

/**
 * Created by rogerio on 16/12/16.
 */
public class CustomViewHolder extends RecyclerView.ViewHolder {
    private ViewDataBinding mViewDataBinding;

    public CustomViewHolder(ItemRepositorieBinding viewDataBinding) {
        super(viewDataBinding.getRoot());
        this.mViewDataBinding = viewDataBinding;
    }

    public ViewDataBinding getViewDataBinding() {
        return mViewDataBinding;
    }
}
