package com.rogeriocs.githublist.api;

import com.rogeriocs.githublist.models.PullRequest;
import com.rogeriocs.githublist.models.Searches;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Callback;

/**
 * Created by rogerio on 16/12/16.
 */

public class GitHubApi {
    static Service serviceApi=HelperService.initRetrofit();

    public static void setServiceApiTest(Service serviceApiParameters){
        serviceApi = serviceApiParameters;
    }
    public static Map<String, String> getQueryStringSearch(String q, String sort,int page) {
        Map<String, String> queryString = new HashMap<String, String>();
        queryString.put(Service.QueryStringSearch.Q, q);
        queryString.put(Service.QueryStringSearch.SORT,sort);
        queryString.put(Service.QueryStringSearch.PAGE,sort);

        return queryString;
    }

    public static void getListRepositories(Map<String, String> queryStrings,Callback<Searches> callBack){
        serviceApi.listRepositories(queryStrings).enqueue(callBack);
    }

    public static void getListPullRequests(String owner, String repo, Callback<ArrayList<PullRequest>> callBack){
        serviceApi.listPullRequests(owner,repo).enqueue(callBack);
    }

}
