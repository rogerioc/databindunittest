package com.rogeriocs.githublist.models.viewModels;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.util.Log;
import android.view.View;

import com.rogeriocs.githublist.activity.PullsRequestsActivity;
import com.rogeriocs.githublist.models.Item;

/**
 * Created by rogerio on 17/12/16.
 */

public class RepositorieViewModel extends BaseObservable  {
    public static final String tag=RepositorieViewModel.class.getSimpleName();
    private final Item item;
    private final Context context;

    public RepositorieViewModel(Context context, Item item) {
        this.item=item;
        this.context = context;
    }

    public String getName(){
        return item.getName();
    }
    public String getDescription(){
        return item.getDescription();
    }

    public  String getOwnerImage(){
        return item.getOwner().getAvatar();
    }

    public String getOwnerName(){
        return  item.getOwner().getLogin();
    }

    public View.OnClickListener onClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(tag,"Click");
                Intent intent = new Intent(context, PullsRequestsActivity.class);
                intent.putExtra(PullsRequestsActivity.ITEM,item);
                ((Activity )context).startActivity(intent);
            }
        };
    }
}
