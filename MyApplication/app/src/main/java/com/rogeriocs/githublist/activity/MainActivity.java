package com.rogeriocs.githublist.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.android.annotations.NonNull;
import com.github.pwittchen.infinitescroll.library.InfiniteScrollListener;
import com.rogeriocs.githublist.R;
import com.rogeriocs.githublist.adapter.EndlessRecyclerOnScrollListener;
import com.rogeriocs.githublist.adapter.RepositoriesAdapter;
import com.rogeriocs.githublist.api.GitHubApi;
import com.rogeriocs.githublist.models.Item;
import com.rogeriocs.githublist.models.Searches;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private final String tag=MainActivity.class.getSimpleName();
    public static final String LANGUAGE_JAVA = "language:Java";
    public static final String SORT = "stars";
    public int PAGE = 1;

    @BindView(R.id.repositoriesList)
    RecyclerView repositoriesListView;
    private Unbinder unBinder;
    private List<Item> repositories;
    private RepositoriesAdapter repositoriesAdapter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unBinder=ButterKnife.bind(this);
        layoutManager= new LinearLayoutManager(this);
        repositoriesListView.setLayoutManager(layoutManager);
        repositories = new ArrayList<>();
        repositoriesAdapter = new RepositoriesAdapter(this,repositories);
        repositoriesListView.setAdapter(repositoriesAdapter);


        getData(PAGE);
        repositoriesListView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                getData(current_page);
            }
        });
    }

    private void getData(int page) {

        GitHubApi.getListRepositories(GitHubApi.getQueryStringSearch(LANGUAGE_JAVA, SORT, page), new Callback<Searches>() {
            @Override
            public void onResponse(Call<Searches> call, Response<Searches> response) {
                Searches searches = response.body();
                Log.i(tag,"Total = "+searches.getTotalCount());
                repositoriesAdapter.setData(searches.getItems());
            }

            @Override
            public void onFailure(Call<Searches> call, Throwable t) {
                Log.e(tag,"Erro = "+t.getMessage());

            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unBinder.unbind();
    }


}
