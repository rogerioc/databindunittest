package com.rogeriocs.githublist.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.rogeriocs.githublist.databinding.ItemPullrequestBinding;
import com.rogeriocs.githublist.models.PullRequest;
import com.rogeriocs.githublist.models.viewModels.PullRequestView;

import java.util.ArrayList;

/**
 * Created by rogerio on 20/12/16.
 */

public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsViewHolder> {
    ArrayList<PullRequest> pullRequests;
    Context context;

    public PullRequestsAdapter(Context context,ArrayList<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
        this.context = context;
    }

    @Override
    public PullRequestsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPullrequestBinding viewDataBinding = ItemPullrequestBinding.inflate(LayoutInflater.from(parent.getContext())
                , parent, false);
        return new PullRequestsViewHolder(viewDataBinding);
    }

    @Override
    public void onBindViewHolder(PullRequestsViewHolder holder, int position) {
        ItemPullrequestBinding itemPullrequestBinding = (ItemPullrequestBinding) holder.getViewDataBinding();
        itemPullrequestBinding.setPullRequestView(new PullRequestView(context,pullRequests.get(position)));
    }

    @Override
    public int getItemCount() {
        return pullRequests==null?0:pullRequests.size();
    }
}
