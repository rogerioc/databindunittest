package com.rogeriocs.githublist.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by rogerio on 20/12/16.
 */

public class User implements Serializable{
    private String login;
    @SerializedName("avatar_url")
    private String avatar;

    public String getLogin() {
        return login;
    }

    public User setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getAvatar() {
        return avatar;
    }

    public User setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }
}
