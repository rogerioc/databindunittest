package com.rogeriocs.githublist.binder;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.rogeriocs.githublist.R;

/**
 * Created by rogerio on 20/12/16.
 */

public class DataBinder {

    @BindingAdapter({"bind:avatarUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Context context = view.getContext();
            Glide.with(context)
                    .load(imageUrl)
                    .placeholder(R.drawable.ic_avatar_default)
                    .error(R.drawable.ic_avatar_default)
                    .into(view);
        }
    }
}
