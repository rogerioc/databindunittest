package com.rogeriocs.githublist.test;

import com.rogeriocs.githublist.models.Item;
import com.rogeriocs.githublist.models.Owner;
import com.rogeriocs.githublist.models.PullRequest;
import com.rogeriocs.githublist.models.User;

import java.util.UUID;

/**
 * Created by rogerio on 17/12/16.
 */
public class HelperMockModels {
    public static String generateRandomString() {
        return UUID.randomUUID().toString();
    }
    public static Item createMockRepositorie() {
        Item item = new Item();
        item.setName(generateRandomString());
        item.setDescription(generateRandomString());
        Owner owner = new Owner();
        owner.setLogin(generateRandomString());
        owner.setAvatar(generateRandomString());
        item.setOwner(owner);
        return item;
    }

    public static PullRequest createMockPullRequest() {
        PullRequest pullRequest = new PullRequest();
        pullRequest.setBody(generateRandomString());
        pullRequest.setCreated(generateRandomString());
        pullRequest.setTitle(generateRandomString());
        pullRequest.setUrl(generateRandomString());
        User user=new User();
        user.setLogin(generateRandomString());
        user.setAvatar(generateRandomString());
        pullRequest.setUser(user);
        return pullRequest;
    }
}
