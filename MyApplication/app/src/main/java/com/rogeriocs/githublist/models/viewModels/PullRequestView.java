package com.rogeriocs.githublist.models.viewModels;

import android.content.Context;
import android.databinding.BaseObservable;

import com.rogeriocs.githublist.models.PullRequest;

/**
 * Created by rogerio on 20/12/16.
 */

public class PullRequestView extends BaseObservable {
    private final PullRequest pullrequest;
    private final Context context;

    public PullRequestView(Context context, PullRequest pullRequest) {
        this.pullrequest=pullRequest;
        this.context = context;
    }

    public String getUserName(){
        if(this.pullrequest.getUser()!=null)
            return this.pullrequest.getUser().getLogin();
        return "";
    }

    public String getTitle(){
        return this.pullrequest.getTitle();
    }

    public String getBody(){
        return this.pullrequest.getBody();
    }


    public String getUserUrl(){
        if(this.pullrequest.getUser()!=null)
            return this.pullrequest.getUser().getAvatar();
        return "";
    }
}
