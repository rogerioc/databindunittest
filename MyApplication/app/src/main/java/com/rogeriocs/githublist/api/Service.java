package com.rogeriocs.githublist.api;

import com.rogeriocs.githublist.models.PullRequest;
import com.rogeriocs.githublist.models.Searches;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by rogerio on 16/12/16.
 */

public interface Service {
    public class QueryStringSearch {
        public static final String Q="q";
        public static final String SORT="sort";
        public static final String PAGE="page";
    }
    @Headers({
            "User-Agent: Awesome-Octocat-App"
    })
    @GET("search/repositories")
    Call<Searches> listRepositories(@QueryMap Map<String, String> options);

    @Headers({
            "User-Agent: Awesome-Octocat-App"
    })
    @GET("repos/{owner}/{repo}/pulls")
    Call<ArrayList<PullRequest>> listPullRequests(@Path("owner") String owner,@Path("repo") String repo);
}
