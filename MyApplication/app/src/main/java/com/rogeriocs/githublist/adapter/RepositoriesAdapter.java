package com.rogeriocs.githublist.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.rogeriocs.githublist.databinding.ItemRepositorieBinding;
import com.rogeriocs.githublist.models.Item;
import com.rogeriocs.githublist.models.viewModels.RepositorieViewModel;

import java.util.List;

/**
 * Created by rogerio on 16/12/16.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<CustomViewHolder> {

    private List<Item> repositories;
    private final Context context;

    public RepositoriesAdapter(Context context, List<Item> repositories) {
        this.repositories = repositories;
        this.context=context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemRepositorieBinding viewDataBinding = ItemRepositorieBinding.inflate(LayoutInflater.from(parent.getContext())
                , parent, false);
        return new CustomViewHolder(viewDataBinding);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        ItemRepositorieBinding viewDataBinding = (ItemRepositorieBinding) holder.getViewDataBinding();
        viewDataBinding.setRepositorieView(new RepositorieViewModel(context,repositories.get(position)));//setVariable(BR.repositorie, repositories.get(position));

    }

    @Override
    public int getItemCount() {
        return repositories==null?0:repositories.size();
    }

    public void setNewData(List<Item> newData) {
        this.repositories = newData;
        notifyDataSetChanged();
    }

    public void setData(List<Item> data) {
        this.repositories.addAll(data);
        notifyDataSetChanged();
    }
}
