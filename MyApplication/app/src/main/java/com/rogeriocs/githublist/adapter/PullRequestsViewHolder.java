package com.rogeriocs.githublist.adapter;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import com.rogeriocs.githublist.databinding.ItemPullrequestBinding;

/**
 * Created by rogerio on 20/12/16.
 */
public class PullRequestsViewHolder extends RecyclerView.ViewHolder {
    private final ItemPullrequestBinding mViewDataBinding;

    public PullRequestsViewHolder(ItemPullrequestBinding viewDataBinding) {
        super(viewDataBinding.getRoot());
        this.mViewDataBinding = viewDataBinding;
    }
    public ViewDataBinding getViewDataBinding() {
        return mViewDataBinding;
    }
}
