package com.rogeriocs.githublist.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rogerio on 20/12/16.
 */

public class PullRequest {
    private String url;
    private String body;
    @SerializedName("created_at")
    private String created;
    private User user;
    private String title;

    public String getUrl() {
        return url;
    }

    public PullRequest setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getBody() {
        return body;
    }

    public PullRequest setBody(String body) {
        this.body = body;
        return this;
    }

    public String getCreated() {
        return created;
    }

    public PullRequest setCreated(String created) {
        this.created = created;
        return this;
    }

    public User getUser() {
        return user;
    }

    public PullRequest setUser(User user) {
        this.user = user;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public PullRequest setTitle(String title) {
        this.title = title;
        return this;
    }
}
