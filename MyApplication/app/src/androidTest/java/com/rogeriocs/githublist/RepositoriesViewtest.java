package com.rogeriocs.githublist;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.rogeriocs.githublist.activity.MainActivity;
import com.rogeriocs.githublist.api.GitHubApi;
import com.rogeriocs.githublist.api.HelperService;
import com.rogeriocs.githublist.api.Service;
import com.rogeriocs.githublist.models.PullRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class RepositoriesViewtest {

    public static final String tag=RepositoriesViewtest.class.getSimpleName();
    private MockWebServer httpServer;
    private String bodyRepositories = "{\n" +
            "\"total_count\": 2622473,\n" +
            "\"incomplete_results\": false,\n" +
            "\"items\": [\n" +
            "  {\n" +
            "\"id\": 507775,\n" +
            "\"name\": \"elasticsearch\",\n" +
            "\"full_name\": \"elastic/elasticsearch\",\n" +
            "\"owner\": {\n" +
            "\"login\": \"elastic\",\n" +
            "\"id\": 6764390,\n" +
            "\"avatar_url\": \"https://avatars.githubusercontent.com/u/6764390?v=3\",\n" +
            "\"gravatar_id\": \"\",\n" +
            "\"url\": \"https://api.github.com/users/elastic\",\n" +
            "\"html_url\": \"https://github.com/elastic\",\n" +
            "\"followers_url\": \"https://api.github.com/users/elastic/followers\",\n" +
            "\"following_url\": \"https://api.github.com/users/elastic/following{/other_user}\",\n" +
            "\"gists_url\": \"https://api.github.com/users/elastic/gists{/gist_id}\",\n" +
            "\"starred_url\": \"https://api.github.com/users/elastic/starred{/owner}{/repo}\",\n" +
            "\"subscriptions_url\": \"https://api.github.com/users/elastic/subscriptions\",\n" +
            "\"organizations_url\": \"https://api.github.com/users/elastic/orgs\",\n" +
            "\"repos_url\": \"https://api.github.com/users/elastic/repos\",\n" +
            "\"events_url\": \"https://api.github.com/users/elastic/events{/privacy}\",\n" +
            "\"received_events_url\": \"https://api.github.com/users/elastic/received_events\",\n" +
            "\"type\": \"Organization\",\n" +
            "\"site_admin\": false\n" +
            "},\n" +
            "\"private\": false,\n" +
            "\"html_url\": \"https://github.com/elastic/elasticsearch\",\n" +
            "\"description\": \"Open Source, Distributed, RESTful Search Engine\",\n" +
            "\"fork\": false,\n" +
            "\"url\": \"https://api.github.com/repos/elastic/elasticsearch\",\n" +
            "\"forks_url\": \"https://api.github.com/repos/elastic/elasticsearch/forks\",\n" +
            "\"keys_url\": \"https://api.github.com/repos/elastic/elasticsearch/keys{/key_id}\",\n" +
            "\"collaborators_url\": \"https://api.github.com/repos/elastic/elasticsearch/collaborators{/collaborator}\",\n" +
            "\"teams_url\": \"https://api.github.com/repos/elastic/elasticsearch/teams\",\n" +
            "\"hooks_url\": \"https://api.github.com/repos/elastic/elasticsearch/hooks\",\n" +
            "\"issue_events_url\": \"https://api.github.com/repos/elastic/elasticsearch/issues/events{/number}\",\n" +
            "\"events_url\": \"https://api.github.com/repos/elastic/elasticsearch/events\",\n" +
            "\"assignees_url\": \"https://api.github.com/repos/elastic/elasticsearch/assignees{/user}\",\n" +
            "\"branches_url\": \"https://api.github.com/repos/elastic/elasticsearch/branches{/branch}\",\n" +
            "\"tags_url\": \"https://api.github.com/repos/elastic/elasticsearch/tags\",\n" +
            "\"blobs_url\": \"https://api.github.com/repos/elastic/elasticsearch/git/blobs{/sha}\",\n" +
            "\"git_tags_url\": \"https://api.github.com/repos/elastic/elasticsearch/git/tags{/sha}\",\n" +
            "\"git_refs_url\": \"https://api.github.com/repos/elastic/elasticsearch/git/refs{/sha}\",\n" +
            "\"trees_url\": \"https://api.github.com/repos/elastic/elasticsearch/git/trees{/sha}\",\n" +
            "\"statuses_url\": \"https://api.github.com/repos/elastic/elasticsearch/statuses/{sha}\",\n" +
            "\"languages_url\": \"https://api.github.com/repos/elastic/elasticsearch/languages\",\n" +
            "\"stargazers_url\": \"https://api.github.com/repos/elastic/elasticsearch/stargazers\",\n" +
            "\"contributors_url\": \"https://api.github.com/repos/elastic/elasticsearch/contributors\",\n" +
            "\"subscribers_url\": \"https://api.github.com/repos/elastic/elasticsearch/subscribers\",\n" +
            "\"subscription_url\": \"https://api.github.com/repos/elastic/elasticsearch/subscription\",\n" +
            "\"commits_url\": \"https://api.github.com/repos/elastic/elasticsearch/commits{/sha}\",\n" +
            "\"git_commits_url\": \"https://api.github.com/repos/elastic/elasticsearch/git/commits{/sha}\",\n" +
            "\"comments_url\": \"https://api.github.com/repos/elastic/elasticsearch/comments{/number}\",\n" +
            "\"issue_comment_url\": \"https://api.github.com/repos/elastic/elasticsearch/issues/comments{/number}\",\n" +
            "\"contents_url\": \"https://api.github.com/repos/elastic/elasticsearch/contents/{+path}\",\n" +
            "\"compare_url\": \"https://api.github.com/repos/elastic/elasticsearch/compare/{base}...{head}\",\n" +
            "\"merges_url\": \"https://api.github.com/repos/elastic/elasticsearch/merges\",\n" +
            "\"archive_url\": \"https://api.github.com/repos/elastic/elasticsearch/{archive_format}{/ref}\",\n" +
            "\"downloads_url\": \"https://api.github.com/repos/elastic/elasticsearch/downloads\",\n" +
            "\"issues_url\": \"https://api.github.com/repos/elastic/elasticsearch/issues{/number}\",\n" +
            "\"pulls_url\": \"https://api.github.com/repos/elastic/elasticsearch/pulls{/number}\",\n" +
            "\"milestones_url\": \"https://api.github.com/repos/elastic/elasticsearch/milestones{/number}\",\n" +
            "\"notifications_url\": \"https://api.github.com/repos/elastic/elasticsearch/notifications{?since,all,participating}\",\n" +
            "\"labels_url\": \"https://api.github.com/repos/elastic/elasticsearch/labels{/name}\",\n" +
            "\"releases_url\": \"https://api.github.com/repos/elastic/elasticsearch/releases{/id}\",\n" +
            "\"deployments_url\": \"https://api.github.com/repos/elastic/elasticsearch/deployments\",\n" +
            "\"created_at\": \"2010-02-08T13:20:56Z\",\n" +
            "\"updated_at\": \"2016-12-16T19:18:16Z\",\n" +
            "\"pushed_at\": \"2016-12-16T23:31:14Z\",\n" +
            "\"git_url\": \"git://github.com/elastic/elasticsearch.git\",\n" +
            "\"ssh_url\": \"git@github.com:elastic/elasticsearch.git\",\n" +
            "\"clone_url\": \"https://github.com/elastic/elasticsearch.git\",\n" +
            "\"svn_url\": \"https://github.com/elastic/elasticsearch\",\n" +
            "\"homepage\": \"https://www.elastic.co/products/elasticsearch\",\n" +
            "\"size\": 298003,\n" +
            "\"stargazers_count\": 19828,\n" +
            "\"watchers_count\": 19828,\n" +
            "\"language\": \"Java\",\n" +
            "\"has_issues\": true,\n" +
            "\"has_downloads\": true,\n" +
            "\"has_wiki\": false,\n" +
            "\"has_pages\": false,\n" +
            "\"forks_count\": 6816,\n" +
            "\"mirror_url\": null,\n" +
            "\"open_issues_count\": 1207,\n" +
            "\"forks\": 6816,\n" +
            "\"open_issues\": 1207,\n" +
            "\"watchers\": 19828,\n" +
            "\"default_branch\": \"master\",\n" +
            "\"score\": 1\n" +
            "}\n" +
            "]\n" +
            "}";
    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule(MainActivity.class);

    @Before
    public void setUp() throws Exception {
        httpServer = new MockWebServer();
        httpServer.start();
        Service service = HelperService.setUrlToTest(httpServer.url("/").toString());
        GitHubApi.setServiceApiTest(service);
        Intents.init();
    }

    @After
    public void tearDown() throws Exception {
        httpServer.shutdown();
        Intents.release();
    }
    @Test
    public void showRepositoryList() throws Exception {
        onView(withId(R.id.repositoriesList)).check(matches(isDisplayed()));
    }

    @Test
    public void testGetTheFirstRepo() throws InterruptedException {

        mockData();

        onView(withRecyclerView(R.id.repositoriesList).atPosition(0))
                .check(matches(hasDescendant(withText("elasticsearch"))));

    }

    @Test
    public void testTapRepo() throws InterruptedException {
        mockData();
        onView(withRecyclerView(R.id.repositoriesList).atPosition(0))
                .perform(click());


    }

    private void mockData() {
        httpServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(bodyRepositories));
        Intent intent = new Intent();
        mainActivityActivityTestRule.launchActivity(intent);
    }

    public static RecyclerViewMatcher withRecyclerView(final int recyclerViewId) {
        return new RecyclerViewMatcher(recyclerViewId);
    }

}
